#!/usr/bin/env bash
SPLIT_TEMPLATE_URL="----REPLACE HERE WITH YOUR CREDENTIALS----"
SDLC_USER_EMAIL="----REPLACE HERE WITH YOUR CREDENTIALS----"
SDLC_USER_NAME="----REPLACE HERE WITH YOUR CREDENTIALS----"
REPO_NOT_FOUND_MESSAGE="fatal: Could not read from remote repository"
SSH_FAIL_MESSAGE="ssh_exchange_identification"
FOLDERS=($(ls -d Services/*  | tr '\n' ' ')) # You may also want change the folders


function print_usage(){
	echo "usage: $0 [-t <tag>]"
}

while getopts ":t:h" opt; do
	case ${opt} in
		t)
			TAG=${OPTARG}
			;;
		h)
			print_usage
			exit 0
			;;
		\?)
			echo "unknown option: -$OPTARG" >&2
			print_usage
			exit 1
			;;
		:)
			echo "option -$OPTARG requires an argument." >&2
			print_usage
			exit 1
			;;
	esac
done


function main(){
	START_TIME="$(date +%s)"

	echo "========================================"
	echo "SPLITTING STARTED"
	echo "========================================"

	set_git_config

	for FOLDER in "${FOLDERS[@]}"; do
		split_module ${FOLDER}
	done

	cleanup

	END_TIME="$(date +%s)"
	echo "Script Execution Time: $((END_TIME-START_TIME)) seconds"
}

function set_git_config() {
	echo "========================================"
	echo "GIT CONFIG"
	echo "========================================"

	git config --global push.default simple
	git config --global user.email ${SDLC_USER_EMAIL}
	git config --global user.name ${SDLC_USER_NAME}
}

function split_module() {
	echo "========================================"
	echo "SPLITTING ${1}"
	echo "========================================"

	FOLDER=$1
	MODULE_NAME=${FOLDER/\//-}
	MODULE_NAME=${MODULE_NAME,,}

	create_repo ${MODULE_NAME}

	if [ -z ${TAG+x} ]; then
		commit ${FOLDER}
		push master
	else
		tag
		push ${TAG}
	fi

	rm -rf .splitter
}

function create_repo() {
	echo "========================================"
	echo "LOCAL REPO CREATION for $1"
	echo "========================================"

	printf -v SPLIT_REPO_URL ${SPLIT_TEMPLATE_URL} $1

	OUTPUT=$(git clone ${SPLIT_REPO_URL} .splitter 2>&1)
	RETURN_CODE=$?
	echo ${OUTPUT}

	if [[ ${OUTPUT} = *"${SSH_FAIL_MESSAGE}"* ]]; then
		echo "Could not clone due to ssh connection lost."
		echo "Retrying..."
		OUTPUT=$(git clone ${SPLIT_REPO_URL} .splitter 2>&1)
		RETURN_CODE=$?
		echo ${OUTPUT}
	fi

	if [[ ${OUTPUT} = *"${REPO_NOT_FOUND_MESSAGE}"* ]]; then
		create_repo_at_upstream ${SPLIT_REPO_URL}
	elif [ ${RETURN_CODE} -ne 0 ]
	then
		exit ${RETURN_CODE}
	fi
}

function create_repo_at_upstream() {
	echo "========================================"
	echo "REMOTE REPO CREATION: $1"
	echo "========================================"

	FOLDER=$1

	mkdir .splitter
	cd .splitter
	git init
	git commit --allow-empty -m "initial commit"
	git push --set-upstream  $1 master
	git remote add origin $1
	cd -
}

function tag() {
	echo "========================================"
	echo "CREATING TAG"
	echo "========================================"

	cd .splitter

	git tag ${TAG}
	cd -
}

function commit() {
	echo "========================================"
	echo "CREATING COMMIT"
	echo "========================================"

	FOLDER=$1

	before_split ${FOLDER}

	LAST_COMMIT_MESSAGE=$(git log -1 --pretty=%B)

	# removes all files and folders except .git folder
	cd .splitter
	ls -a .  | tr ' ' '\n' | grep -v ^..$ | grep -v ^.$ | grep -v ^.git$ | xargs rm -rf
	cd -

	cp -rf ${FOLDER}/. .splitter

	cd .splitter
	git add .
	git commit -m "${LAST_COMMIT_MESSAGE}"

	cd -
}

function before_split() {
	echo "========================================"
	echo "'before-split.sh' EXECUTION AT $1"
	echo "========================================"

	echo "executing before-split.sh at $1"
	cd $1
	chmod 750 before-split.sh
	./before-split.sh > /dev/null
	cd -
	echo "executed before-split.sh at $1"
}

function push() {
	echo "========================================"
	echo "PUSHING $1"
	echo "========================================"

	ref=$1

	cd .splitter

	OUTPUT=$(git push origin ${ref} 2>&1)
	echo ${OUTPUT}
	if [[ ${OUTPUT} = *"${SSH_FAIL_MESSAGE}"* ]]; then
		echo "Could not push due to ssh connection lost."
		echo "Retrying..."
		OUTPUT=$(git push origin ${ref} 2>&1)
		echo ${OUTPUT}
	fi

	cd -
}

function cleanup() {
	echo "========================================"
	echo "CLEANUP"
	echo "========================================"
	git reset --hard HEAD
	git clean -fd
}

main

