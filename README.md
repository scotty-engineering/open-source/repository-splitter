# Repository Splitter

`split.sh` splits your monolithic repository to new repositories. 

## Configuration

Edit variables in `split.sh` for your needs:

- `SPLIT_TEMPLATE_URL` template url of  new split repositories. 
Exmaple: `git@gitlab.com:my-group-for-split/%s.git`
- `SDLC_USER_EMAIL`: email of user that pushes to new split repositories 
- `SDLC_USER_NAME`: username of user that pushes to new split repositories
- `FOLDERS`: folder paths for each new repository.
Example: ('ServiceA' 'ServiceB') or ($(ls -d Services/*  | tr '\n' ' '))

## Example CI yml

```yaml
stages:
- split

before_script:
- mkdir -p ~/.ssh
- echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
- chmod 700 ~/.ssh/id_rsa
- eval "$(ssh-agent -s)"
- ssh-add ~/.ssh/id_rsa
- ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts

push:
  image: buildpack-deps:jessie-scm
  stage: split
  script: ./split.sh
  only: [master]

tag:
  image: buildpack-deps:jessie-scm
  stage: split
  script: ./split.sh -t $CI_COMMIT_REF_NAME
  only:
  - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  except:
  - branches
```
 
## You May Also Like

- [splitsh](https://github.com/splitsh/lite)
